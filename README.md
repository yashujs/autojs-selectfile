## 使用场景

调用手机自带的文件管理  选择文件

## 效果展示

<img src="选择文件.png" alt="选择文件" style="zoom:50%;" />



## autojs版本号

![autojs版本号](autojs版本号.png)

## 功能

1. 可任意选择图片, 音频, 视频
2. 可以更换imgView的图片
3. 可以播放音频
4. 可以播放视频

## 你将学到以下知识

1. 调用文件管理选择文件
2. 接收文件管理返回的数据
3. 增加或者删除子view
4. 使用文件头判断文件类型
5. uri转文件路径

---

## 微信公众号 AutoJsPro教程



![officialaccount.jpg](officialaccount.jpg)



## QQ群



747748653



![给我个面子小图](给我个面子小图.jpg)